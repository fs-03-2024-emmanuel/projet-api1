var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.status(200).json({
    data:[]
  });
});

router.post('/', function(req, res, next) {
    res.status(200).json({
      data:req.body

    });
  });

router.get('/:id', function(req, res, next) {
    res.status(200).json({
        data:{},
        id:req.params.id
    });
  });

  
  router.put('/:id', function(req, res, next) {
    res.status(200).json({
        data:req.body,
        id:req.params.id
    });
  });

  router.delete('/:id', function(req, res, next) {
    res.status(200).json({
        id:req.params.id
    });
  });
  

module.exports = router;
